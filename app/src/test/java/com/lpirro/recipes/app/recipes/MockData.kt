package com.lpirro.recipes.app.recipes

import com.lpirro.recipes.network.models.Ingredient
import com.lpirro.recipes.network.models.Recipe

object MockData {
    fun generateMockedRecipesList(): List<Recipe> {

        return listOf(
                Recipe("Beef BBQ 1", generateIngredientList(), listOf("Step 1", "Step 2", "Step 3"), listOf(0, 10, 0), "", ""),
                Recipe("Beef BBQ 2", generateIngredientList(), listOf("Step 1", "Step 2", "Step 3"), listOf(0, 5, 10), "", ""),
                Recipe("Beef BBQ 3", generateIngredientList(), listOf("Step 1", "Step 2", "Step 3"), listOf(20, 60, 120), "", ""),
                Recipe("Beef BBQ 4", generateIngredientList(), listOf("Step 1", "Step 2", "Step 3"), listOf(20, 60, 0), "", "")
        )
    }

    fun generateMockSingleRecipe(): Recipe {
        return generateMockedRecipesList()[0]
    }

    private fun generateIngredientList(): List<Ingredient> {
        val mockedIngredient = Ingredient("1", "Beef", "Meat")
        val ingredientList = ArrayList<Ingredient>()
        ingredientList.add(mockedIngredient)
        return ingredientList
    }
}