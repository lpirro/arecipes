package com.lpirro.recipes.app.recipes

import com.lpirro.recipes.app.util.DataFilter
import com.lpirro.recipes.network.TestSchedulerProvider
import com.lpirro.recipes.network.api.ApiService
import com.lpirro.recipes.network.api.Repository
import com.lpirro.recipes.network.models.Recipe
import com.nhaarman.mockitokotlin2.*
import io.reactivex.Single
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito

class RecipesPresenterTest {
    private lateinit var presenter: RecipesPresenter
    private var view = mock<RecipesContract.RecipesView>()
    private val service = mock<ApiService>()
    private var repo = mock<Repository>()

    @Before
    fun setup() {
        repo.apiService = service
        presenter = RecipesPresenter(repo, TestSchedulerProvider())
        presenter.attachView(view)
    }

    @Test
    fun `get recipes should show recipes`() {
        val list = MockData.generateMockedRecipesList()
        val single: Single<List<Recipe>> = Single.just(list)

        doReturn(single).`when`(repo.apiService).getRecipes()

        presenter.loadRecipes()

        verify(view).showRecipes(list)
    }

    @Test
    fun `get recipes should show error message`() {

        doReturn(Single.error<Any>(Exception("dummy exception"))).`when`(repo.apiService).getRecipes()

        presenter.loadRecipes()
        verify(view).showError(any())
    }

    @Test
    fun `get recipes should show loading`() {
        val list = MockData.generateMockedRecipesList()
        val single: Single<List<Recipe>> = Single.just(list)
        doReturn(single).`when`(repo.apiService).getRecipes()

        val inOrder = Mockito.inOrder(view)
        presenter.loadRecipes()

        inOrder.verify(view).showLoadingIndicator(true)
        inOrder.verify(view).showLoadingIndicator(false)

    }

    @Test
    fun `click on recipe should open recipe detail`() {
        val recipe = MockData.generateMockSingleRecipe()
        presenter.openRecipeDetail(recipe)
        verify(view).showRecipesDetailUi(recipe)
    }

    @Test
    fun `search recipes should show filered recipes`() {
        val list = MockData.generateMockedRecipesList()

        presenter.searchRecipe("Beef BBQ 2", list)

        verify(view).showFilteredRecipes(any())
    }

    @Test
    fun `search recipes not in the list should show empty view`() {
        val list = MockData.generateMockedRecipesList()

        presenter.searchRecipe("Recipe Not In The List", list)

        verify(view).showEmptyView(true)
    }

    @Test
    fun `time filter should show filered recipes`() {
        val list = MockData.generateMockedRecipesList()

        presenter.timeFilter(list, 10..20)

        verify(view).showFilteredRecipes(any())
    }

    @Test
    fun `complexity filter should show filered recipes`() {
        val list = MockData.generateMockedRecipesList()

        presenter.filterByComplexity(list, DataFilter.Complexity.HARD)

        verify(view).showFilteredRecipes(any())
    }
}