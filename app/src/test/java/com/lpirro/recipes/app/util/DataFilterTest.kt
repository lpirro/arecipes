package com.lpirro.recipes.app.util

import com.lpirro.recipes.app.recipes.MockData
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.hasItems
import org.junit.Assert.*
import org.junit.Test

class DataFilterTest {

    private val listUnderTest = MockData.generateMockedRecipesList()

    @Test
    fun `perform search should show only beef bbq 2`() {
        val searchQuery = "Beef BBQ 2"
        val result = DataFilter.search(listUnderTest, searchQuery)

        assertThat(result[0].name, `is`("Beef BBQ 2"))
        assertThat(result.size, `is`(1))
    }

    @Test
    fun `perform filter by time should show only values between 0 and 10 minutes`() {
        val timeRange = 0..10
        val result = DataFilter.timeFilter(listUnderTest, timeRange)

        val expectedRecipe = listUnderTest[0]

        assertThat(result, hasItems(expectedRecipe))
    }

    @Test
    fun `perform filter by time should show only values between 10 and 20 minutes`() {
        val timeRange = 10..20
        val result = DataFilter.timeFilter(listUnderTest, timeRange)

        val expectedRecipe = listUnderTest[1]

        assertThat(result, hasItems(expectedRecipe))
    }

    @Test
    fun `perform filter by time should show only values between 20 and 1000 minutes`() {
        val timeRange = 20..1000
        val result = DataFilter.timeFilter(listUnderTest, timeRange)

        val expectedRecipe = listUnderTest[2]

        assertThat(result, hasItems(expectedRecipe))
    }

    @Test
    fun `perform filter by complexity EASY should show only EASY recipes`() {
        val result = DataFilter.getRecipeByComplexity(listUnderTest, DataFilter.Complexity.EASY)

        val expectedRecipes = arrayListOf(listUnderTest[0], listUnderTest[1])
        assertThat(result, hasItems(expectedRecipes[0], expectedRecipes[1]))
    }

    @Test
    fun `perform filter by complexity MEDIUM should show only MEDIUM recipes`() {
        val result = DataFilter.getRecipeByComplexity(listUnderTest, DataFilter.Complexity.MEDIUM)

        val expectedRecipe = listUnderTest[3]
        assertThat(result, hasItems(expectedRecipe))
    }

    @Test
    fun `perform filter by complexity HARD should show only HARD recipes`() {
        val result = DataFilter.getRecipeByComplexity(listUnderTest, DataFilter.Complexity.HARD)

        val expectedRecipe = listUnderTest[2]
        assertThat(result, hasItems(expectedRecipe))
    }
}