package com.lpirro.recipes.network.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Ingredient(val quantity: String, val name: String, val type: String): Parcelable