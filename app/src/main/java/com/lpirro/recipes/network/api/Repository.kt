package com.lpirro.recipes.network.api

import com.lpirro.recipes.network.models.Recipe
import io.reactivex.Single

open class Repository(var apiService: ApiService = ApiServiceFactory.makeApiService()) {

    fun getRecipes(): Single<List<Recipe>> {
        return apiService.getRecipes()
    }
}
