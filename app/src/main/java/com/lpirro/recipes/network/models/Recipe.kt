package com.lpirro.recipes.network.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Recipe(val name: String, val ingredients: List<Ingredient>, val steps: List<String>, val timers: List<Int>,
                  val imageURL: String, val originalURL: String): Parcelable {

    val timeToCook: Int get() {
        var total = 0
        timers.forEach { total += it }
        return total
    }
}