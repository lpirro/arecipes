package com.lpirro.recipes.network.api

import com.lpirro.recipes.network.models.Recipe
import io.reactivex.Single
import retrofit2.http.GET

interface ApiService {

    @GET("sampleapifortest/recipes.json")
    fun getRecipes(): Single<List<Recipe>>

}