package com.lpirro.recipes.network.api

import com.google.gson.GsonBuilder
import com.lpirro.recipes.BuildConfig
import com.lpirro.recipes.app.RecipeApplication
import okhttp3.Cache
import okhttp3.CacheControl
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

object ApiServiceFactory {

    private const val BASE_URL = BuildConfig.BASE_URL

    fun makeApiService(): ApiService {
        return makeApiService(makeOkHttpClient())
    }

    private fun makeApiService(okHttpClient: OkHttpClient): ApiService {

        val retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(makeGson()))
                .client(okHttpClient)
                .build()

        return retrofit.create(ApiService::class.java)
    }

    private fun makeOkHttpClient() : OkHttpClient {
        val cacheSize = (5 * 1024 * 1024).toLong()
        val cache = Cache(RecipeApplication.application.cacheDir, cacheSize)

        val networkCacheInterceptor = Interceptor { chain ->
            val response = chain.proceed(chain.request())

            val cacheControl = CacheControl.Builder()
                    .maxAge(1, TimeUnit.HOURS)
                    .build()

            response.newBuilder()
                    .header("Cache-Control", cacheControl.toString())
                    .build()
        }
        
        val okHttpClientBuilder = OkHttpClient.Builder()
        .cache(cache)
        .addNetworkInterceptor(networkCacheInterceptor)
        .retryOnConnectionFailure(false)
                .addNetworkInterceptor(makeLoggingInterceptor())

        return okHttpClientBuilder.build()

    }

    private fun makeGson() = GsonBuilder()
                .serializeNulls()
                .create()

    private fun makeLoggingInterceptor(): HttpLoggingInterceptor {
        val loggingInterceptor = HttpLoggingInterceptor()
        loggingInterceptor.level =
                if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY
                else HttpLoggingInterceptor.Level.NONE

        return loggingInterceptor
    }
}
