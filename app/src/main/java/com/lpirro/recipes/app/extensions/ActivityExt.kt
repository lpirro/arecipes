package com.lpirro.recipes.app.extensions

import android.app.Activity
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.navigation.Navigation
import androidx.navigation.ui.NavigationUI
import com.lpirro.recipes.R

fun Activity.setToolbar(toolbar: Toolbar){
    if(this is AppCompatActivity){
        val navController = Navigation.findNavController(this, R.id.navHostFragment)
        this.setSupportActionBar(toolbar)
        NavigationUI.setupWithNavController(toolbar, navController)
    }
}