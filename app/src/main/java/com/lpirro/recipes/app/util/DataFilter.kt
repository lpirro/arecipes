package com.lpirro.recipes.app.util

import com.lpirro.recipes.network.models.Recipe
import kotlin.math.roundToInt

object DataFilter {

    fun timeFilter(recipes: List<Recipe>, intRange: IntRange): List<Recipe> {
        val recipe = ArrayList<Recipe>()

        recipes.forEach {
            if (it.timeToCook in intRange) {
                recipe.add(it)
            }
        }

        return recipe
    }

    fun search(recipes: List<Recipe>, query: String): ArrayList<Recipe> {
        val result = arrayListOf<Recipe>()

        recipes.forEachIndexed { recipeIndex, recipe ->

            if (recipe.name.contains(query, true)) {
                if (!result.contains(recipe))
                    result.add(recipe)
            }

            recipe.ingredients.forEach {
                if (it.name.contains(query, true)) {
                    if (!result.contains(recipes[recipeIndex]))
                        result.add(recipes[recipeIndex])
                }
            }

            recipe.steps.forEach {
                if (it.contains(query, true)) {
                    if (!result.contains(recipes[recipeIndex]))
                        result.add(recipes[recipeIndex])
                }
            }
        }

        return result
    }

    enum class Complexity(val value: IntRange) {
        EASY(0..45),
        MEDIUM(45..100),
        HARD(100..1000)
    }

    fun getRecipeByComplexity(recipes: List<Recipe>, complexity: Complexity): ArrayList<Recipe> {
        val recipeFiltered = ArrayList<Recipe>()

        recipes.forEach {
            when (complexity) {
                Complexity.EASY -> {
                    if (getComplexityScore(it) in Complexity.EASY.value)
                        recipeFiltered.add(it)
                }
                Complexity.MEDIUM -> {
                    if (getComplexityScore(it) in Complexity.MEDIUM.value)
                        recipeFiltered.add(it)
                }
                Complexity.HARD -> {
                    if (getComplexityScore(it) in Complexity.HARD.value)
                        recipeFiltered.add(it)
                }
            }
        }

        return recipeFiltered
    }

    private fun getComplexityScore(recipe: Recipe): Int {
        val timeToCookScore = when (recipe.timeToCook) {
            in 0..20 -> 25
            in 20..40 -> 50
            in 40..80 -> 75
            else -> 100
        }
        val ingredientScore = recipe.ingredients.size * 1.15
        val stepScore = recipe.steps.size * 1.50

        val score = timeToCookScore + ingredientScore + stepScore
        return score.roundToInt()
    }
}