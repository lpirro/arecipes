package com.lpirro.recipes.app.recipes

import com.lpirro.recipes.app.util.DataFilter
import com.lpirro.recipes.mvp.BasePresenter
import com.lpirro.recipes.network.SchedulerProvider
import com.lpirro.recipes.network.api.Repository
import com.lpirro.recipes.network.models.Recipe
import io.reactivex.disposables.CompositeDisposable

class RecipesPresenter(private val repository: Repository, private val schedulerProvider: SchedulerProvider) :
        BasePresenter<RecipesContract.RecipesView>(), RecipesContract.Presenter {

    private val compositeDisposable = CompositeDisposable()

    override fun unsubscribe() {
        compositeDisposable.clear()
    }

    override fun loadRecipes() {
        view?.showEmptyView(false)
        view?.showLoadingIndicator(true)

        val disposable = repository.getRecipes()
                .subscribeOn(schedulerProvider.ioScheduler())
                .observeOn(schedulerProvider.uiScheduler())
                .doFinally { view?.showLoadingIndicator(false) }
                .subscribe(
                        { posts -> view?.showRecipes(posts) },
                        { error -> view?.showError(error) }
                )

        compositeDisposable.add(disposable)
    }

    override fun openRecipeDetail(clickedRecipe: Recipe) {
        view?.showRecipesDetailUi(clickedRecipe)
    }

    override fun searchRecipe(query: String, recipes: List<Recipe>) {
        view?.showEmptyView(false)

        val filteredRecipes = DataFilter.search(recipes, query)

        when {
            filteredRecipes.isEmpty() -> view?.showEmptyView(true)
            else -> view?.showFilteredRecipes(filteredRecipes)
        }
    }

    override fun timeFilter(recipes: List<Recipe>, intRange: IntRange) {
        view?.showEmptyView(false)

        val filteredRecipes = DataFilter.timeFilter(recipes, intRange)

        when {
            filteredRecipes.isEmpty() -> view?.showEmptyView(true)
            else -> view?.showFilteredRecipes(filteredRecipes)
        }
    }

    override fun filterByComplexity(recipes: List<Recipe>, complexity: DataFilter.Complexity) {
        view?.showEmptyView(false)

        val filteredRecipes = DataFilter.getRecipeByComplexity(recipes, complexity)

        when {
            filteredRecipes.isEmpty() -> view?.showEmptyView(true)
            else -> view?.showFilteredRecipes(filteredRecipes)
        }
    }
}