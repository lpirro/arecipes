package com.lpirro.recipes.app.recipedetail

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.lpirro.recipes.R
import com.lpirro.recipes.app.GlideApp
import com.lpirro.recipes.app.base.BaseFragment
import com.lpirro.recipes.app.extensions.setToolbar
import com.lpirro.recipes.app.recipedetail.adapter.IngredientsAdapter
import com.lpirro.recipes.app.recipedetail.adapter.StepsAdapter
import com.lpirro.recipes.network.models.Recipe
import kotlinx.android.synthetic.main.fragment_recipe_detail.*

class RecipeDetailFragment: BaseFragment(){

    override fun layoutId() = R.layout.fragment_recipe_detail

    private val ingredientsAdapter: IngredientsAdapter by lazy {
        IngredientsAdapter()
    }

    private val stepsAdapter: StepsAdapter by lazy {
        StepsAdapter()
    }

    private lateinit var recipe: Recipe

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        recipe = RecipeDetailFragmentArgs.fromBundle(arguments).recipe

        activity?.setToolbar(toolbar)
        collapsingToolbarLayout.title = recipe.name

        initIngredientsRecyclerView(rvIngredients)
        initStepsRecyclerView(rvSteps)

        ingredientsAdapter.setData(recipe.ingredients)
        stepsAdapter.setData(recipe.steps, recipe.timers)

        GlideApp.with(ivRecipeDetailImage).load(recipe.imageURL).into(ivRecipeDetailImage)
    }

    private fun initIngredientsRecyclerView(rv: RecyclerView){
        rv.layoutManager = LinearLayoutManager(context)
        rv.adapter = ingredientsAdapter
    }

    private fun initStepsRecyclerView(rv: RecyclerView){
        rv.layoutManager = LinearLayoutManager(context)
        rv.adapter = stepsAdapter
    }

}