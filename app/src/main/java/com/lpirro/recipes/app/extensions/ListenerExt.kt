package com.lpirro.recipes.app.extensions

import android.view.View
import android.widget.AdapterView
import android.widget.Spinner

fun Spinner.itemSelectedListener(block: (position: Int) -> Unit){
    this.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
        override fun onNothingSelected(parent: AdapterView<*>?) {}

        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
            block(position)
        }
    }
}