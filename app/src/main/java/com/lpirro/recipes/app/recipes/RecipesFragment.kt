package com.lpirro.recipes.app.recipes

import android.opengl.Visibility
import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.View
import android.widget.Toast
import androidx.appcompat.widget.SearchView
import androidx.navigation.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.lpirro.recipes.R
import com.lpirro.recipes.app.base.BaseFragment
import com.lpirro.recipes.app.extensions.itemSelectedListener
import com.lpirro.recipes.app.extensions.setToolbar
import com.lpirro.recipes.app.recipes.adapter.RecipesAdapter
import com.lpirro.recipes.app.util.DataFilter
import com.lpirro.recipes.network.AppSchedulerProvider
import com.lpirro.recipes.network.api.Repository
import com.lpirro.recipes.network.models.Recipe
import kotlinx.android.synthetic.main.fragment_receipes.*

const val ARG_RECIPES_ON_SCREEN = "arg_recipes_on_screen"
const val ARG_RECIPES_ORIGINAL_DATA = "arg_recipes_original_data"

class RecipesFragment : BaseFragment(), RecipesContract.RecipesView, RecipeClickListener, SearchView.OnQueryTextListener,
        SearchView.OnCloseListener {

    override fun layoutId() = R.layout.fragment_receipes

    private val presenter: RecipesPresenter by lazy {
        RecipesPresenter(Repository(), AppSchedulerProvider())
    }

    private val adapter: RecipesAdapter by lazy {
        RecipesAdapter(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        activity?.setToolbar(toolbar)

        if (savedInstanceState == null)
            loadData()
        else {
            val recipeLastShowedData = savedInstanceState.getParcelableArrayList<Recipe>(ARG_RECIPES_ON_SCREEN)!!
            val recipeOriginalData = savedInstanceState.getParcelableArrayList<Recipe>(ARG_RECIPES_ORIGINAL_DATA)!!
            adapter.setData(recipeLastShowedData)
            adapter.setOriginalData(recipeOriginalData)
        }

        rvRecipes.layoutManager = GridLayoutManager(context, 2, RecyclerView.VERTICAL, false)
        rvRecipes.adapter = adapter
        swipeRefresh.setOnRefreshListener {
            loadData()
        }

        spinnerTime.itemSelectedListener {
            performItemSelectedTimeFilter(it)
        }

        spinnerComplexity.itemSelectedListener {
            performItemSelectedComplexityFilter(it)
        }

    }

    override fun onRecipeClick(recipe: Recipe) {
        presenter.openRecipeDetail(recipe)
    }

    override fun showRecipesDetailUi(recipe: Recipe) {
        val recipeDetailDirections =
                RecipesFragmentDirections.actionRecipesFragmentToRecipeDetailFragment(recipe)

        view?.findNavController()?.navigate(recipeDetailDirections)
    }

    override fun showRecipes(recipes: List<Recipe>) {
        rvRecipes.scheduleLayoutAnimation()
        adapter.setData(recipes)
    }

    override fun showFilteredRecipes(recipes: List<Recipe>) {
        rvRecipes.scheduleLayoutAnimation()
        adapter.setFiltered(recipes)
    }

    override fun showLoadingIndicator(active: Boolean) {
        swipeRefresh.isRefreshing = active
    }

    override fun showError(error: Throwable) {
        Toast.makeText(context, error.localizedMessage, Toast.LENGTH_SHORT).show()
    }

    override fun onQueryTextSubmit(query: String): Boolean {
        presenter.searchRecipe(query, adapter.getOriginalData())
        resetSpinners()
        return false
    }

    override fun onQueryTextChange(newText: String): Boolean {
        return false
    }

    override fun onClose(): Boolean {
        loadData()
        return false
    }

    override fun showEmptyView(show: Boolean) {
        if(show)
            emptyView.visibility = View.VISIBLE
        else
            emptyView.visibility = View.GONE
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_main, menu)

        val searchView = menu.findItem(R.id.action_search).actionView as SearchView
        searchView.setOnCloseListener(this)
        searchView.setOnQueryTextListener(this)

        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)

        val currentShowedData = ArrayList(adapter.getData())
        val originalData = ArrayList(adapter.getOriginalData())
        outState.putParcelableArrayList(ARG_RECIPES_ON_SCREEN, currentShowedData)
        outState.putParcelableArrayList(ARG_RECIPES_ORIGINAL_DATA, originalData)
    }

    override fun onResume() {
        super.onResume()
        presenter.attachView(this)
    }

    override fun onStop() {
        super.onStop()
        presenter.unsubscribe()
    }

    private fun performItemSelectedTimeFilter(position: Int) {
        when (position) {
            0 -> adapter.restoreOriginalData()
            1 -> presenter.timeFilter(adapter.getOriginalData(), 0..10)
            2 -> presenter.timeFilter(adapter.getOriginalData(), 10..20)
            3 -> presenter.timeFilter(adapter.getOriginalData(), 20..1000)
        }
    }

    private fun performItemSelectedComplexityFilter(position: Int) {
        when (position) {
            0 -> adapter.restoreOriginalData()
            1 -> presenter.filterByComplexity(adapter.getOriginalData(), DataFilter.Complexity.EASY)
            2 -> presenter.filterByComplexity(adapter.getOriginalData(), DataFilter.Complexity.MEDIUM)
            3 -> presenter.filterByComplexity(adapter.getOriginalData(), DataFilter.Complexity.HARD)
        }
    }

    private fun resetSpinners() {
        spinnerTime.setSelection(0)
        spinnerComplexity.setSelection(0)
    }

    private fun loadData() {
        presenter.loadRecipes()
        resetSpinners()
    }
}