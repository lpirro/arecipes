package com.lpirro.recipes.app.recipes

import com.lpirro.recipes.app.util.DataFilter
import com.lpirro.recipes.mvp.View
import com.lpirro.recipes.network.models.Recipe

interface RecipesContract {

    interface RecipesView: View {
        fun showRecipesDetailUi(recipe: Recipe)
        fun showRecipes(recipes: List<Recipe>)
        fun showFilteredRecipes(recipes: List<Recipe>)
        fun showEmptyView(show: Boolean)
    }

    interface Presenter {
        fun loadRecipes()
        fun openRecipeDetail(clickedRecipe: Recipe)
        fun searchRecipe(query: String, recipes: List<Recipe>)
        fun timeFilter(recipes: List<Recipe>, intRange: IntRange)
        fun filterByComplexity(recipes: List<Recipe>, complexity: DataFilter.Complexity)
    }
}