package com.lpirro.recipes.app.recipes

import com.lpirro.recipes.network.models.Recipe

interface RecipeClickListener {
    fun onRecipeClick(recipe: Recipe)
}