package com.lpirro.recipes.app

import android.app.Application

class RecipeApplication: Application(){

    companion object {
        lateinit var application: Application
    }

    override fun onCreate() {
        super.onCreate()
        application = this
    }
}