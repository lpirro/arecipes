package com.lpirro.recipes.app.recipes.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.lpirro.recipes.R
import com.lpirro.recipes.app.GlideApp
import com.lpirro.recipes.app.recipes.RecipeClickListener
import com.lpirro.recipes.network.models.Recipe
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.view_recipe.*
import java.util.*

class RecipesAdapter(private val listener: RecipeClickListener): RecyclerView.Adapter<RecipesAdapter.RecipeVH>() {

    private var originalData: List<Recipe> = ArrayList()
    private var data: List<Recipe> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecipeVH {
        return RecipeVH(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.view_recipe, parent, false)
        )
    }

    override fun getItemCount() = data.size

    override fun onBindViewHolder(holder: RecipeVH, position: Int) {
        val currentRecipe = data[position]

        val ingredientsText = holder.itemView.context
            .getString(R.string.ingredients_number, currentRecipe.ingredients.size.toString())

        val timeToCookText = holder.itemView.context
            .getString(R.string.time_to_cook_number, currentRecipe.timeToCook.toString())

        holder.tvRecipeName.text = currentRecipe.name
        holder.tvIngredientsNumber.text = ingredientsText
        holder.tvCookTime.text = timeToCookText

        GlideApp.with(holder.itemView)
                .load(currentRecipe.imageURL).centerCrop().into(holder.ivRecipeImage)

        holder.itemView.setOnClickListener { listener.onRecipeClick(currentRecipe) }
    }

    fun setData(data: List<Recipe>) {
        this.originalData = data
        this.data = data
        notifyDataSetChanged()
    }

    fun setFiltered(data: List<Recipe>){
        this.data = data
        notifyDataSetChanged()
    }

    fun restoreOriginalData() {
        this.data = originalData
        notifyDataSetChanged()
    }

    fun setOriginalData(originalData: List<Recipe>){
        this.originalData =  originalData
    }

    fun getData() = data

    fun getOriginalData() = originalData

    inner class RecipeVH(override val containerView: View) : RecyclerView.ViewHolder(containerView), LayoutContainer
}