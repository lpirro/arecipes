package com.lpirro.recipes.app.recipedetail.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.lpirro.recipes.R
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.view_steps.view.*
import java.util.*

private const val VIEW_TYPE_STEP_HEADER = 0
private const val VIEW_TYPE_STEP = 1

class StepsAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var stepData: List<String> = ArrayList()
    private var timeData: List<Int> = ArrayList()

    override fun getItemViewType(position: Int): Int {
        if(position == 0)
            return VIEW_TYPE_STEP_HEADER

        return VIEW_TYPE_STEP
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            VIEW_TYPE_STEP -> StepVH(LayoutInflater.from(parent.context)
                    .inflate(R.layout.view_steps, parent, false))
            else -> StepHeaderVH(LayoutInflater.from(parent.context)
                    .inflate(R.layout.view_steps_header, parent, false))
        }
    }

    override fun getItemCount() = stepData.size + 1

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        when(holder.itemViewType){
            VIEW_TYPE_STEP -> (holder as StepVH).bind(stepData[position-1],timeData[position-1])
        }
    }

    fun setData(data: List<String>, times: List<Int>) {
        this.stepData = data
        this.timeData = times
        notifyDataSetChanged()
    }

    inner class StepVH(override val containerView: View) : RecyclerView.ViewHolder(containerView), LayoutContainer{
        fun bind(step: String, timeToCook: Int){

            val timeToCookText = itemView.context
                    .getString(R.string.time_to_cook_number_short, timeToCook.toString())

            itemView.tvStepCount.text = adapterPosition.toString()
            itemView.tvStepDescription.text = step
            itemView.tvStepCookTime.text = timeToCookText
        }
    }

    inner class StepHeaderVH(override val containerView: View) : RecyclerView.ViewHolder(containerView), LayoutContainer
}