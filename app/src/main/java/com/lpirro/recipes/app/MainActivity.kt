package com.lpirro.recipes.app

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.lpirro.recipes.R

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}
