package com.lpirro.recipes.app.recipedetail.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.lpirro.recipes.R
import com.lpirro.recipes.network.models.Ingredient
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.view_ingredients.*
import kotlinx.android.synthetic.main.view_ingredients_header.*
import java.util.*

private const val VIEW_TYPE_INGREDIENT_HEADER = 0
private const val VIEW_TYPE_INGREDIENT = 1

class IngredientsAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var data: List<Ingredient> = ArrayList()

    override fun getItemViewType(position: Int): Int {
        if(position == 0)
            return VIEW_TYPE_INGREDIENT_HEADER

        return VIEW_TYPE_INGREDIENT
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            VIEW_TYPE_INGREDIENT -> IngredientVH(LayoutInflater.from(parent.context)
                    .inflate(R.layout.view_ingredients, parent, false))
            else -> IngredientHeaderVH(LayoutInflater.from(parent.context)
                    .inflate(R.layout.view_ingredients_header, parent, false))
        }
    }

    override fun getItemCount() = data.size + 1

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        when(holder.itemViewType){
            VIEW_TYPE_INGREDIENT -> (holder as IngredientVH).bind(data[position-1])
            VIEW_TYPE_INGREDIENT_HEADER -> (holder as IngredientHeaderVH).bind()
        }
    }

    fun setData(data: List<Ingredient>) {
        this.data = data
        notifyDataSetChanged()
    }

    inner class IngredientVH(override val containerView: View) : RecyclerView.ViewHolder(containerView), LayoutContainer {
        fun bind(ingredient: Ingredient){
            tvIngredientName.text = ingredient.name
            tvIngredientQuantity.text = ingredient.quantity
        }
    }

    inner class IngredientHeaderVH(override val containerView: View) : RecyclerView.ViewHolder(containerView), LayoutContainer {
        fun bind(){
            tvIngredientHeader.text = itemView.context.getString(R.string.ingredients_header, data.size.toString())
        }
    }
}