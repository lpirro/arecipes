package com.lpirro.recipes.mvp

interface View {

    fun showLoadingIndicator(active: Boolean)

    fun showError(error: Throwable)
}
