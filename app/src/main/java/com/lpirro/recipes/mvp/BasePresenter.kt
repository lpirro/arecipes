package com.lpirro.recipes.mvp

abstract class BasePresenter<V: View> {

    protected var view: V? = null

    fun attachView(view: V) {
        this.view = view
    }

    abstract fun unsubscribe()
}
